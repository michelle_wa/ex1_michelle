#include "queue.h"
#include<iostream>

//create dynamic array according to maxsize
void initQueue(queue* q, unsigned int size)
{
	q->_elements = new int[size];
	q->_maxSize = size;
	q->_last = 0;
	q->_first = 0;
}

//free the memory of the array
void cleanQueue(queue* q)
{
	delete[] q->_elements;
	delete q;
}

//A function that checks if the stack is empty
bool isEmpty(queue* q)
{
	return last == 0;
}

//A function that checks if the stack is full
bool isFull(queue* q)
{
	return last == _maxSize;
}

//Adds an element to the end of the list
void enqueue(queue* q, unsigned int newValue)
{
	if (isFull(q))
	{
		std::cout << "queue is Full" << std::endl;
	}
	else
	{
		q->_elements[q->_last] = newValue;
	}
	q->_last++;
}

//Extracts an element from the beginning of the list
int dequeue(queue* q)
{
	int element1 = 0;
	int i = 0;
	if (isEmpty(q))
	{
		element1 = -1;
	}
	else
	{
		element1 = q->_elements[0];
	}
	for (i = 0; i < q->_last - 1; i++)
	{
		q->_elements[i + 1] = q->_elements[i];
	}
	q->_last--;
	return element1;
}

