#pragma once
#ifndef QUEUE_H
#define QUEUE_H


/* a queue contains positive integer values. */
typedef struct stack
{
	int _element;
	stack* next;
} stack;

void add(stack** head, unsigned int value);
void deleteHead(stack** head);
#endif /* QUEUE_H */